%global _empty_manifest_terminate_build 0
Name:		python-gunicorn
Version:	23.0.0
Release:	1
Summary:	WSGI HTTP Server for UNIX
License:	MIT
URL:		https://pypi.org/project/gunicorn
Source0:	https://files.pythonhosted.org/packages/34/72/9614c465dc206155d93eff0ca20d42e1e35afc533971379482de953521a4/gunicorn-23.0.0.tar.gz
BuildArch:	noarch

BuildRequires: python3-pip python3-wheel
BuildRequires: python3-hatchling python3-hatch-vcs

%description
Gunicorn(Green Unicorn) is a Python WSGI HTTP Server for UNIX. It's a pre-fork
worker model ported from Ruby's Unicorn_ project. The Gunicorn server is broadly
compatible with various web frameworks, simply implemented, light on server
resource usage, and fairly speedy.

%package -n python3-gunicorn
Summary:	WSGI HTTP Server for UNIX
Provides:	    python-gunicorn
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools

%description -n python3-gunicorn
Gunicorn(Green Unicorn) is a Python WSGI HTTP Server for UNIX. It's a pre-fork
worker model ported from Ruby's Unicorn_ project. The Gunicorn server is broadly
compatible with various web frameworks, simply implemented, light on server
resource usage, and fairly speedy.

%package help
Summary:	Development documents and examples for gunicorn
Provides:	python3-gunicorn-doc

%description help
Development documents and examples for gunicorn.

%prep
%autosetup -n gunicorn-%{version}

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-gunicorn -f filelist.lst
%dir %{python3_sitelib}/*
%{_usr}/lib/python3.11/site-packages/gunicorn/*

%files help -f doclist.lst
%{_pkgdocdir}

%changelog
* Wed Aug 21 2024 guochao <guochao@kylinos.cn> - 23.0.0-1
- Upgrade to version 23.0.0
- Minor docs fixes
- Worker_class parameter accepts a class
- Fix deadlock if request terminated during chunked parsing 

* Wed Jun 05 2024 liuzhilin <liuzhilin@kylinos.cn> - 22.0.0-1
- Upgrade to version 22.0.0 to fix CVE-2024-1135

* Wed Jan 10 2024 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 21.2.0-1
- Upgrade to version 21.2.0

* Thu Jun 23 2022 SimpleUpdate Robot <tc@openeuler.org> - 20.1.0-1
- Upgrade to version 20.1.0

* Fri Dec 18 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
